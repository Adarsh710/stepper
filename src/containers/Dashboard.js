import React from "react";
import PropTypes from "prop-types";
import { withRouter } from 'react-router-dom'
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  container: {
    paddingLeft: "20px",
    paddingRight: "20px",
    width: "100%"
  },
  button: {
    margin: theme.spacing(1)
  }
});

function Dashboard(props) {
    const { classes } = props;

    const navToStepper = (stepper) => {
        props.history.push("/stepper?stepper="+ stepper)
    }

    return (
      <div className={classes.container}>
        <Button variant="contained" className={classes.button} onClick={ (e) => {navToStepper("stepperOne")}}>
          Stepper 1
        </Button>
        <Button variant="contained" color="primary" className={classes.button} onClick={ (e) => {navToStepper("stepperTwo")}}>
          Stepper 2
        </Button>
      </div>
    );
}

// eslint-disable-next-line react/no-typos
Dashboard.PropTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(Dashboard));
