import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({});

class StepFive extends React.Component {
   render(){
       return (<div>
             <Paper>
                    <b>Step5 stepper 1</b>
             </Paper>
       </div>)
   }

}

StepFive.propTypes = {
    classes: PropTypes.object.isRequired,
  };
export default withStyles(styles)(StepFive);