export const stepperData = {
    "stepperOne": ["WHEN & WHERE", "VEHICLE SELECTION", "ADD ONS", "PERSONAL DETAILS & SUMMARY", "PAYMENT"],
    "stepperTwo": ["DETAILS", "SPECIFICATION", "ENGINE & TRANSMISSION", "WHEELS & TYRES", "FLUIDS"]
}